import svdf.writer as wrtr
import svdf.parser as prsr


class Types:
    @staticmethod
    def object():
        return b'\x00'

    @staticmethod
    def str():
        return b'\x01'

    @staticmethod
    def int():
        return b'\x02'


class Special:
    @staticmethod
    def object_end():
        return b'\x08'

    @staticmethod
    def string_end():
        return b'\x00'

    @staticmethod
    def property_end():
        return b'\x00'


def int_of(_byte):
    return int.from_bytes(_byte, byteorder='little')


def parse(path):
    file = open(path, "rb")
    data = file.read()
    file.close()

    return prsr.parse_object(prsr.ParseBuffer(data, 0))


def write(obj, path):
    write_data = bytearray()
    wrtr.write_object(write_data, obj)
    file = open(path, "wb")
    file.write(write_data)
    file.close()
