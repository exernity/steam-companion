import svdf


class ParseBuffer(object):
    def __init__(self, data, carriage):
        self.data = data
        self.carriage = carriage


def parse_string(parse_buffer):
    start = parse_buffer.carriage

    while parse_buffer.data[parse_buffer.carriage] != 0:
        parse_buffer.carriage += 1
    parse_buffer.carriage += 1

    return parse_buffer.data[start:parse_buffer.carriage - 1].decode(encoding="utf-8")


def parse_int(parse_buffer):
    parse_buffer.carriage += 4

    return int.from_bytes(parse_buffer.data[parse_buffer.carriage - 4:parse_buffer.carriage],
                          byteorder='little', signed=True)


def parse_object(parse_buffer):
    obj = {}

    while True:
        value_type = parse_buffer.data[parse_buffer.carriage]
        parse_buffer.carriage += 1

        if value_type == svdf.int_of(svdf.Special.object_end()):
            break

        property_name = parse_string(parse_buffer)

        if value_type == svdf.int_of(svdf.Types.str()):
            property_value = parse_string(parse_buffer)
        elif value_type == svdf.int_of(svdf.Types.int()):
            property_value = parse_int(parse_buffer)
        else:
            property_value = parse_object(parse_buffer)

        obj[property_name] = property_value

        if parse_buffer.carriage >= len(parse_buffer.data):
            break

    return obj
