import svdf


def write_object(write_buffer, value):
    for key, value in value.items():
        if type(value) is dict:
            write_buffer += svdf.Types.object() + \
                            bytearray(key, encoding="utf-8") + \
                            svdf.Special.property_end()
            write_object(write_buffer, value)
        elif type(value) is str:
            write_buffer += svdf.Types.str() + \
                            bytearray(key, encoding="utf-8") + \
                            svdf.Special.property_end() + \
                            bytearray(value, encoding="utf-8") + \
                            svdf.Special.string_end()
        elif type(value) is int:
            write_buffer += svdf.Types.int() + \
                            bytearray(key, encoding="utf-8") + \
                            svdf.Special.property_end() + \
                            bytearray(value.to_bytes(4, byteorder='little', signed=True))
    write_buffer += svdf.Special.object_end()
