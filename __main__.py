import svdf


def add_shortcut(obj, app_id, name, exe, start_dir, icon):
    shortcut_obj = {
        "appid": app_id,
        "AppName": name,
        "Exe": exe,
        "StartDir": start_dir,
        "icon": icon,
        "ShortcutPath": "",
        "LaunchOption": "",
        "isHidden": 0,
        "AllowDesktopConfig": 1,
        "AllowOverlay": 1,
        "OpenVR": 0,
        "Devkit": 0,
        "DevkitGameID": "",
        "LastPlayTime": 0
    }

    obj["shortcuts"][str(obj["shortcuts"].__len__())] = shortcut_obj


def main():
    obj = svdf.parse("./shortcuts.vdf")
    svdf.write(obj, "./shortcuts_exit.vdf")


if __name__ == "__main__":
    main()
